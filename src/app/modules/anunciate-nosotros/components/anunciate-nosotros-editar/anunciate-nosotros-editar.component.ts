import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {EmethValidators} from "@ceneval_plataforma_emeth/ngx-emeth";
import {SelectionModel} from "@angular/cdk/collections";

@Component({
  selector: 'app-anunciate-nosotros-editar',
  templateUrl: './anunciate-nosotros-editar.component.html',
  styleUrls: ['./anunciate-nosotros-editar.component.scss']
})
export class AnunciateNosotrosEditarComponent implements OnInit {
  form: FormGroup<Formulario>;
  catIdiomas: Idioma[] = [];
  idiomasSelection: SelectionModel<number> = new SelectionModel<number>(true);

  constructor() {
    this.form = new FormGroup<Formulario>({
      nombre: new FormControl<string>('', {validators: EmethValidators.notBlank, nonNullable: true}),
      nombreEmpresa: new FormControl<string>('', {validators: EmethValidators.notBlank, nonNullable: true}),
      email: new FormControl<string>('', {validators: [EmethValidators.notBlank, Validators.email], nonNullable: true}),
      telefono: new FormControl<string>('', {validators: EmethValidators.notBlank, nonNullable: true}),
      categoriaNegocio: new FormControl<string>('', {validators: Validators.required, nonNullable: true}),
      entidadFederativa: new FormControl<string>('', {validators: Validators.required, nonNullable: true}),
      idiomasHablados: new FormControl<number[]>([], {nonNullable: true}),
      acercaEmpresa: new FormControl<string>('', {validators: EmethValidators.notBlank, nonNullable: true})
    });
  }

  ngOnInit(): void {
    this.cargarCatalogos();
    this.cargarDatos();
  }

  idiomaChange(idIdioma: number, checked: boolean) {
    if (checked)
      this.idiomasSelection.select(idIdioma);
    else
      this.idiomasSelection.deselect(idIdioma);
    this.form.controls.idiomasHablados.markAllAsTouched();
  }

  cargarDatos() {
    const registro = {
      nombre: 'David Josue Rodríguez Chávez'
    };
    this.form.controls.nombre.setValue(registro.nombre);
  }

  cargarCatalogos() {
    this.catIdiomas = [
      {
        id: 1,
        nombre: 'Árabe'
      },
      {
        id: 2,
        nombre: 'Chino'
      },
      {
        id: 3,
        nombre: 'Inglés'
      },
      {
        id: 4,
        nombre: 'Francés'
      },
      {
        id: 5,
        nombre: 'Alemán'
      },
      {
        id: 6,
        nombre: 'Hindi/Urdu'
      },
      {
        id: 7,
        nombre: 'Japonés'
      },
      {
        id: 8,
        nombre: 'Coreano'
      },
      {
        id: 9,
        nombre: 'Español'
      },
    ];
  }

}

type Idioma = {
  id: number;
  nombre: string;
}

type Formulario = {
  nombre: FormControl<string>;
  nombreEmpresa: FormControl<string>;
  email: FormControl<string>;
  telefono: FormControl<string>;
  categoriaNegocio: FormControl<string>;
  entidadFederativa: FormControl<string>;
  idiomasHablados: FormControl<number[]>;
  acercaEmpresa: FormControl<string>;
}
