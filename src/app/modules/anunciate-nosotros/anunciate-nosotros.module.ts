import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnunciateNosotrosRoutingModule } from './anunciate-nosotros-routing.module';
import {AnunciateNosotrosComponent} from "./components/anunciate-nosotros/anunciate-nosotros.component";
import {TranslateModule} from "@ngx-translate/core";
import { AnunciateNosotrosGestionarComponent } from './components/anunciate-nosotros-gestionar/anunciate-nosotros-gestionar.component';
import {NgxEmethModule} from "@ceneval_plataforma_emeth/ngx-emeth";
import {MatTableModule} from "@angular/material/table";
import {MatButtonModule} from "@angular/material/button";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatIconModule} from "@angular/material/icon";
import { AnunciateNosotrosEditarComponent } from './components/anunciate-nosotros-editar/anunciate-nosotros-editar.component';
import {ReactiveFormsModule} from "@angular/forms";
import {MatCheckboxModule} from "@angular/material/checkbox";


@NgModule({
  declarations: [
    AnunciateNosotrosComponent,
    AnunciateNosotrosGestionarComponent,
    AnunciateNosotrosEditarComponent
  ],
  imports: [
    CommonModule,
    AnunciateNosotrosRoutingModule,
    TranslateModule,
    NgxEmethModule,
    MatTableModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    ReactiveFormsModule,
    MatCheckboxModule
  ]
})
export class AnunciateNosotrosModule { }
